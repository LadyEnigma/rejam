﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int initialHealth = 100;
    public int currentHealth;
    public int currentDamage = 0;
    public Slider healthBar;
    public Image damageImg;
    public AudioClip hitClip;
    public AudioClip deathClip;
    public AudioClip powerupClip;
    public float flashSpeed = 5.0f;
    public Color flashColor = new Color (1f, 0f, 0f, 0.5f);

    private Animator anim;
    private AudioSource playerAudio;
    private PlayerMovement playerMovement;
    private PlayerAttack playerAttack;
    private bool isDead;
    private bool isDamaged;

    void Awake()
    {
        anim = GetComponent<Animator> ();
        playerAudio = GetComponent<AudioSource> ();
        playerMovement = GetComponent<PlayerMovement> ();
        playerAttack = GetComponent<PlayerAttack> ();
        currentHealth = initialHealth;
    }

    void Update()
    {
        if (isDamaged)
        {
            damageImg.color = flashColor;
        }
        else
        {
            damageImg.color = Color.Lerp (damageImg.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        isDamaged = false;
    }

    public void TakeDamage (int amount)
    {
        isDamaged = true;
        currentHealth -= amount;
        currentDamage += amount;
        healthBar.value = currentDamage;
        playerAudio.Play ();
        if (currentHealth <= 0 && !isDead)
        {
            Death ();
        }
    }

    void Death()
    {
        isDead = true;
        anim.SetTrigger ("Die");
        playerAudio.clip = deathClip;
        playerAudio.Play ();
        playerMovement.enabled = false;
        playerAttack.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Health")
        {
            currentHealth += 20;
            currentDamage -= 20;
            playerAudio.clip = powerupClip;
            playerAudio.Play ();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Health")
        {
            playerAudio.clip = hitClip;
        }
    }
}
