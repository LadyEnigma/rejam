﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour
{
    public int initialHealth = 100;
    public int currentHealth;
    public float stunTime = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;
    public GameObject fish;

    private Animator anim;
    private AudioSource enemyAudio;
    private ParticleSystem hitParticles;
    private CapsuleCollider _collider;
    private bool isStunned;

    void Awake()
    {
        anim = GetComponent<Animator> ();
        enemyAudio = GetComponent<AudioSource> ();
        hitParticles = GetComponentInChildren<ParticleSystem> ();
        _collider = GetComponent<CapsuleCollider> ();

        currentHealth = initialHealth;
    }

    void Update()
    {
        //TODO: setup timer when stunned before waking back up
    }

    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        if (isStunned)
            return;
        enemyAudio.Play();
        currentHealth -= amount;
        //hitParticles.transform.position = hitPoint;
        //hitParticles.Play ();
        if (currentHealth <= 0)
        {
            Stunned ();
        }
    }

    void Stunned ()
    {
        isStunned = true;
        _collider.isTrigger = true;
        anim.SetTrigger ("Stunned");
        hitParticles.Play ();
        enemyAudio.clip = deathClip;
        enemyAudio.Play ();
        ScoreManager.score += scoreValue;
        Instantiate (fish, transform.position, transform.rotation);
    }
}
