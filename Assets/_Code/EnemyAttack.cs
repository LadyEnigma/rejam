﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    public float attackDelay = 0.5f;
    public int attackDamage = 10;

    private Animator anim;
    private GameObject player;
    private PlayerHealth playerHealth;
    private EnemyHealth enemyHealth;
    private bool isClose;
    private float timer;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag ("Player");
        playerHealth = player.GetComponent<PlayerHealth> ();
        enemyHealth = GetComponent<EnemyHealth> ();
        anim = GetComponent<Animator> ();
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == player)
        {
            isClose = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            isClose = false;
        }
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= attackDelay && isClose && enemyHealth.currentHealth > 0)
        {
            Attack ();
            anim.SetTrigger ("Attack");
        }
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger ("PlayerDead");
        }
    }

    void Attack()
    {
        timer = 0f;
        if (playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage (attackDamage);
        }
    }
}
