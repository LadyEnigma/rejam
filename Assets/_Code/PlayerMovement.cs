﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{
    public float speed = 6.0f;

    private Vector3 movement;
    private Animator anim;
    private Rigidbody body;

    void Awake()
    {
        anim = GetComponent<Animator> ();
        body = GetComponent<Rigidbody> ();
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw ("Horizontal");
        float v = Input.GetAxisRaw ("Vertical");

        Move (h, v);
        Turn (h, v);
        Animating (h, v);
    }

    void Move(float h, float v)
    {
        movement.Set (h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;
        body.MovePosition (transform.position + movement);
    }

    void Turn(float h, float v)
    {
        Vector3 turnDirection = Vector3.right * Input.GetAxis ("Horizontal") + Vector3.forward * Input.GetAxis ("Vertical");
        if (turnDirection.sqrMagnitude > 0.0f)
        {
            transform.rotation = Quaternion.LookRotation (turnDirection, Vector3.up);
        }
    }

    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool ("isWalking", walking);
    }
}
