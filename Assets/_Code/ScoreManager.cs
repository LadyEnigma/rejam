﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static int score;

    private Text scoreText;

    void Awake()
    {
        scoreText = GetComponent<Text> ();
        score = 0;
    }

    void Update()
    {
        scoreText.text = "Score: " + score;
    }
}
