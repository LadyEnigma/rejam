﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour
{
    public void StartGame()
    {
        Application.LoadLevel ("MainLevel");
    }

    public void ViewCredits()
    {
        Application.LoadLevel ("Credits");
    }

    public void BackToMenu()
    {
        Application.LoadLevel ("Menu");
    }
}
