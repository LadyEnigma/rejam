﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour
{
    public float attackDelay = 0.5f;
    public int attackDamage = 10;

    private Animator anim;
    private GameObject enemy;
    private PlayerHealth playerHealth;
    private EnemyHealth enemyHealth;
    private bool isClose;
    private bool setOnce = false;
    private float timer;
    private Ray frontRay;
    private RaycastHit slapHit;

    void Awake ()
    {
        playerHealth = GetComponent<PlayerHealth> ();
        anim = GetComponent<Animator> ();
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.tag == "Enemies")
        {
            enemy = other.gameObject;
            enemyHealth = enemy.GetComponent<EnemyHealth> ();
            isClose = true;
            setOnce = false;
        }
    }

    void OnTriggerExit (Collider other)
    {
        if (other.tag == "Enemies")
        {
            enemyHealth = null;
            enemy = null;
            isClose = false;
        }
    }

    void Update ()
    {
        timer += Time.deltaTime;
        if (Input.GetButton("Fire1") && enemy != null && timer >= attackDelay && isClose && playerHealth.currentHealth > 0)
        {
            Attack ();
            anim.SetTrigger ("Attack");
        }
        if (!setOnce)
        {
            if (enemy != null && enemyHealth.currentHealth <= 0)
            {
                anim.SetTrigger ("EnemyDead");
                Debug.Log ("Enemy is dead");
                setOnce = true;
                enemy = null;
            } 
        }
    }

    void Attack ()
    {
        timer = 0f;
        frontRay.origin = new Vector3 (transform.position.x, transform.position.y + .5f, transform.position.z);
        frontRay.direction = transform.forward;
        Debug.Log ("Attacking?");
        if (Physics.Raycast (frontRay.origin, frontRay.direction, out slapHit))
        {
            Debug.Log (slapHit);
            if (enemyHealth.currentHealth > 0)
            {
                enemyHealth.TakeDamage (attackDamage, slapHit.point);
                Debug.Log ("Should hit enemy");
            }
        }
    }
}
